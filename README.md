# Mail_protector

> Email address obfuscation in javascript to protect against robots and spams

[![Build Status](https://travis-ci.org/kbrsh/wing.svg?branch=master)](https://gitlab.com/A1CY0N/mail_protector/pipelines)
[![Packagist](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/A1CY0N/mail_protector/blob/master/LICENSE)

Mail protector is a great and super light-weight (just ~250 bytes) tool to prevent bots from collecting your email address on your site. The emails are encoded in your html sources and are decoded in javascript by the browser. 

All you have to do is import the javascript script at the end of your html file and encode your email addresses.

## DISCLAIMER

**Beware, this script does not encrypt your email addresses, it's just obfuscation against basic bots.**

## How it works

The tool protects your email addresses using a two-digit key.

Each pair of characters of the e-mail address is converted to hexadecimal and is xored with the key in hexadecimal.

The final result is to concatenate the key and then the result of the xor.

## Import

### Clone the Repo and Install Dependencies:

```bash
git clone https://gitlab.com/A1CY0N/mail_protector
# Check integrity
diff <(sha384sum mail-protect.js) <(cat mail-protect.js.sha384)
diff <(sha384sum mail-protect.min.js) <(cat mail-protect.min.js.sha384)
```

```html
<script async src="mail-protect.min.js" integrity="sha384-336ad723cb074fc53c06b7a417ec503c8d81990537280dc8d2b767db7ca69423d1166454f79bf9e656a55bce65349e3e" referrerpolicy="same-origin"></script>
```

OR

```html
<script async src="mail-protect.js" integrity="sha384-8b32d653f3c848b280739c3412f5b09105c9d7a2f8dab04e0b9c255acd481bbcce21bc3d57bf8e88d1304c47e37e5848" referrerpolicy="same-origin"></script>
```

### Include via a CDN

> TO-DO

## How to use

First of all you have to encode all your email addresses on your website :
    
- Use the [web tool]() for that (Need JS on your browser)
- Or use `encode.js`

You must then replace your email addresses by the encoded ones.

Lastly you have to import the script at the end of your html page.

## Example

```js
>> encode("example@exemple.org", 46)
"2e4b564f435e424b6e4b564f435e424b10415c49"

>> decodeEmail("2e4b564f435e424b6e4b564f435e424b10415c49")
"example@exemple.org"
```

## Release History

* 0.1.0
    * First version of website to encode

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Find a bug?

Submit it in the [issues](https://gitlab.com/A1CY0N/mail_protector/issues)

## License

Licensed under the [MIT License](https://gitlab.com/A1CY0N/mail_protector/blob/master/LICENSE) by [A1CY0N](https://a1cy0n.xyz)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/mail_protector>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request