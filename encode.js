function encode(email, key) {
    var encodedKey = key.toString(16);
    var encodedString = make2DigitsLong(encodedKey);
    for (var n = 0; n < email.length; n++) {
        var charCode = email.charCodeAt(n);
        var encoded = charCode ^ key;
        var val = encoded.toString(16);
        encodedString += make2DigitsLong(val);
    }
    return encodedString;
}

function make2DigitsLong(val) {
    return val.length === 1 ?
        '1' + val :
        val;
}